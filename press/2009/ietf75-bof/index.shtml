<!--#include virtual="/ssi/header.include" -->
<!-- Enter custom page information and styles here -->
  <title>July 28, 2009: Xiph.Org Supporting IETF Royalty Free Codecs BoF</title>
</head>
<body>
<!--#include virtual="/common/xiphbar.include" -->

<!--#include virtual="/ssi/pagetop.include" -->
<!--  All your page content goes here  -->

<h3>Xiph.Org Supporting IETF Royalty Free Codecs BoF</h3>

<p>For some years now, Xiph.Org has been gently advocating royalty
free codecs within the IETF (Internet Engineering Task Force) as a
viable and preferred alternative to patent-controlled technologies.
Recently more groups within the IETF have also come forward to support
free codec standardization work with in the IETF. On July 30th at
the next IETF Meeting (IETF 75 in
Stockholm<a href="http://www.ietf.org/meeting/75/">[1]</a>) the IETF
will hold a codecs BoF to take input on whether or not to form a
standards Working Group for royalty free audio codecs.  The creation
of such a Working Group would have a major impact, opening the door to
a Voice over IP stack completely free of licensing and royalty
constraints.  We're seeking to set up the same kind of open
competition and cooperation we've seen on the web that's given rise to
tons of investment and improvement in a very short period of time.

<p>Giving input on the Working Group proposal does not require being
in Sweden for the IETF meeting; Those who cannot attend in person can
have their voice heard via the codec@jabber.ietf.org chat room or by
posting to the codec mailing
list<a href="https://www.ietf.org/mailman/listinfo/codec">[2]</a>.  We
encourage people to attend, comment and discuss in person, via Jabber
and on the mailing lists as opponents of the WG proposal are expected
to be well-organized and out in force.  The important thing is to
clearly demonstrate that there is a large number of people in favor of
creating the new Working Group.

<p>There are currently two draft submissions to be discussed at the
BoF.  The first
draft<a href="http://tools.ietf.org/html/draft-valin-celt-codec-01">[3]</a>
describes the new Xiph.Org CELT
codec<a href="http://www.celt-codec.org/">[4]</a> and the second
describes the Skype SILK
codec<a href="http://tools.ietf.org/html/draft-vos-silk-00">[5]</a>. These
two draft proposals complement each other nicely.  There is also
declared intent to submit the SPIRIT IPMR wideband
codec<a href="http://www.ietf.org/mail-archive/web/codec/current/msg00431.html">[6]</a>.

<p>This Working Group proposal faces stiff opposition from current codec
patent holders who benefit from the status quo that allows them to
control competition from organizations whose business models cannot
support per-channel royalties.  Now is a very good time to speak up on
this issue.

<p>We hope to see you (physically or virtually) in large numbers for
the BoF.  Xiph.Org will be represented in person at the meeting by
Christopher (Monty) Montgomery [Director] and Jean-Marc Valin [CELT
Project Lead]. The BoF will be at 13:00 Stockholm time, with times in
other timezones listed
in <a href="http://tinyurl.com/stockholm-codec-bof">[7]</a>. The IETF
will stream an mp3 audio
feed<a href="http://feed.verilan.com/ietf/stream02.m3u">[8]</a> of the
BoF (and yes, we'll be setting up an Ogg feed of the audio). Please
refer to <a href="http://people.xiph.org/~jm/bof_details.html">this
page[9]</a> for up-to-date information on the BoF, as well as an
update on the location of the Ogg audio feed.

<p>(note: this effort is targeted at real-time audio codecs and
is independent from the HTML5 effort involving Vorbis and Theora
at the W3C)

<p>
[1] <a href="http://www.ietf.org/meeting/75/">http://www.ietf.org/meeting/75/</a><br>
[2] <a href="https://www.ietf.org/mailman/listinfo/codec">https://www.ietf.org/mailman/listinfo/codec</a><br>
[3] <a href="http://tools.ietf.org/html/draft-valin-celt-codec-01">http://tools.ietf.org/html/draft-valin-celt-codec-01</a><br>
[4] <a href="http://www.celt-codec.org/">http://www.celt-codec.org/</a><br>
[5] <a href="http://tools.ietf.org/html/draft-vos-silk-00">http://tools.ietf.org/html/draft-vos-silk-00</a><br>
[6] <a href="http://www.ietf.org/mail-archive/web/codec/current/msg00431.html">http://www.ietf.org/mail-archive/web/codec/current/msg00431.html</a><br>
[7] <a href="http://tinyurl.com/stockholm-codec-bof">http://tinyurl.com/stockholm-codec-bof</a><br>
[8] <a href="http://feed.verilan.com/ietf/stream02.m3u">http://feed.verilan.com/ietf/stream02.m3u</a><br>
[9] <a href="http://people.xiph.org/~jm/bof_details.html">http://people.xiph.org/~jm/bof_details.html</a><br>

<p><i>The Xiph.Org Foundation is a not-for-profit corporation dedicated to
open, unencumbered multimedia technology. Xiph's formats and software
level the playing field for digital media so that all producers and
artists can distribute their work for minimal cost, without
restriction, regardless of affiliation. May contain traces of nuts.</i>

<!--#include virtual="/ssi/pagebottom.include" -->
