Format: 1.0
Source: positron
Version: 1.0b2-1
Binary: positron
Maintainer: Jack Moffitt <jack@xiph.org>
Architecture: all
Standards-Version: 3.5.8
Build-Depends-Indep: debhelper (>> 4.0.0), python (>= 2.1), python-dev (>= 2.1)
Files: 
 4ac173bba5b312b06d80466d491f27e8 83025 positron_1.0b2.orig.tar.gz
 071e315917f29366bd72c69b22e4170a 20 positron_1.0b2-1.diff.gz
